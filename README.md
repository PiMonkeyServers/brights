Brights
=========

Who knows?...

Description
-----------
Adds some bright stuff... but what?


Dependencies
--------------
default (Minetest Game)

Optional Depends
-----------------
farming  
3d_armor  
shields

License
---------

Code: LGPL 2.1 by xenonca  
Media: CC-BY-SA 4.0 by xenonca


Version
---------
v1.0 First Release
